﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Degrees_Converter
{
    class Convert
    {
        public double CF(double val)
        {
            return (val * 9) / 5 + 32;
        }
        public double FC(double val)
        {
            return (val - 32) * 5 / 9;
        }

    }
}
